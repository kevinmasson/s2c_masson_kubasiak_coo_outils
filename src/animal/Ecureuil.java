package animal;

/**
 * Permet de gérer un animal de type écureuil
 */
public class Ecureuil implements Animal{

	/**
	 * Représente les points de vie de l'écurreuil
	 */
	private int pointDeVie;
	
	/**
	 * Représente le stock de noisette de l'écureuil
	 */
	private int noisette;

	/**
	 * Construit un écureuil par défaut
	 */
	public Ecureuil() {
		noisette = 0;
		pointDeVie = 5;
	}
	
	/**
	 * Construit un écureil paramétré
	 * Si le paramètre est négatif, il devient donc nul
	 * @param vie
	 * La vie de l'écureuil
	 */
	public Ecureuil(int vie){
		if(vie<0) vie = 0;
		noisette = 0;
		pointDeVie = vie;
	}
	
	/**
	 * permet de savoir si un animal est mort
	 * 
	 * @return true si l'animal est mort
	 */
	public boolean etreMort(){
		if(pointDeVie==0)
			return true;
		else
			return false;
	}

	/**
	 * fait evoluer l'animal d'un jour l'oblige a se nourrir ou a perdre des
	 * points de vie
	 * 
	 * @return true si l'animal est vivant a la fin du jour
	 */
	public boolean passerUnjour() throws MortDeFaimException{

			if(etreMort())
				throw new MortDeFaimException("L'ecureuil est mort avant d'avoir commencer la journée");
			// Il doit manger une noisette sous peine de perdre 2 pdv
			if(noisette > 0) 
				stockerNourriture(-1);
			else pointDeVie -= 2;
			if(pointDeVie<0)
				pointDeVie=0;
			if(etreMort())
				throw new MortDeFaimException("L'ecureuil est mort avant la fin de la journée");
			// Il perde une noisette dans la foret
			stockerNourriture(-1);			
			return true;
	}

	/**
	 * permet d'ajouter de la nourriture a son stock
	 * 
	 * @param nourriture
	 *            quantite de nourriture stockee
	 */
	public void stockerNourriture(int nourriture){
		noisette += nourriture;
		if(noisette < 0)
			noisette = 0;
	}

	/**
	 * retourne les points de vie de l'animal
	 * @return points de vie de l'animal
	 */
	public int getPv(){
		return pointDeVie;
	}
	
	/**
	 * retourne le stocke de nourriture possede par l'animal
	 * @return nourriture de l'animal
	 */
	public int getStockNourriture(){
		return noisette;
	}
	
	/**
	 * Retourne une chaine représentant l'éccureuil
	 */
	public String toString(){
		return ("Ecureuil - pv:"+pointDeVie+" noisettes:"+noisette);
	}	
}
