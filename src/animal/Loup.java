package animal;

public class Loup implements Animal {
	/**
	 * Réprésente les points de vie du Loup
	 */
	private int pointDeVie;
	/**
	 * Réprésente la nourriture du Loup 
	 */
	private int viande;
		
	
	/**
	 * Constructeur pas défaut
	 */
	public Loup(){
		pointDeVie = 30;
		viande = 0;
	}
	
	public Loup(int pdv){
		if(pdv<0) pdv = 0;
		viande = 0;
		pointDeVie=pdv;
		}
	
	/**
	 * permet de savoir si un animal est mort
	 * 
	 * @return true si l'animal est mort
	 */
	public boolean etreMort() {
		if(pointDeVie==0) return true;
		else return false;
	}

	/**
	 * fait evoluer l'animal d'un jour l'oblige a se nourrir ou a perdre des
	 * points de vie
	 * 
	 * @return true si l'animal est vivant a la fin du jour
	 */
	public boolean passerUnjour() throws MortDeFaimException{
			if(etreMort())
				throw new MortDeFaimException("Le loup n'a pas passé la nuit");
			if(viande >= 1) 
				viande--;
			else pointDeVie -= 4;
			if(pointDeVie<0)
				pointDeVie=0;
			if(etreMort())
				throw new MortDeFaimException("Le loup est mort avant la fin de la journée");
			// il perd la moitié de sa nourriture
			viande = (int)viande/2 ;			
			return true;
	}
	
	/**
	 * permet d'ajouter de la nourriture a son stock
	 * 
	 * @param nourriture
	 *            quantite de nourriture stockee
	 */
	public void stockerNourriture(int nourriture) {
		if(viande+nourriture >=0)viande+=nourriture;
		else viande = 0;
	}

	/**
	 * retourne les points de vie de l'animal
	 * @return points de vie de l'animal
	 */
	public int getPv() {
		return pointDeVie;
	}

	/**
	 * retourne le stocke de nourriture possede par l'animal
	 * @return nourriture de l'animal
	 */
	public int getStockNourriture() {
		return viande;
	}
	
	/**
	 * Retourne une chaine représentant le loup
	 */
	public String toString(){
		return ("Loup - pv:"+pointDeVie+" viande :"+viande);
	}	

	
	
}
