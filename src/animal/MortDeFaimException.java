package animal;

/**
 * Gère le cas où un animal meurt de faim avant la fin de la journée
 */
public class MortDeFaimException extends Exception {

	public MortDeFaimException(String message) {
		super(message);
	}

}
