package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import animal.Ecureuil;
import animal.MortDeFaimException;

public class TestEcureuil {

		// Test constructeur par défaut
	@Test
	public void testConstructeur_1() {
		//Initialisation
		Ecureuil ecu = new Ecureuil();
		int pv = ecu.getPv();
		int nou = ecu.getStockNourriture();
		// Test du nombre de pv;
		assertEquals("Les pv de l'ecureuil devraient etre a 5", 5, pv);
		// Test de la nourriture
		assertEquals("La nourriture de l'ecureuil devraient etre a 0", 0, nou);
	}
	
	//Test constructeur parametre normal
	@Test
	public void testConstructeurParametre1(){
		//Initialisation
		Ecureuil ecu = new Ecureuil(1);
		int pv = ecu.getPv();
		int nou = ecu.getStockNourriture();
		// Test du nombre de pv
		assertEquals("Les pv de l'ecureuil devraient être a 1", 1, pv);
		//Test de la nourriture
		assertEquals("La nourriture devrait être à 0", 0, nou);
	}

	//test constructeur parametre PDV = 0
	@Test
	public void testConstructeurParametre2(){
		//Initialisation
		Ecureuil ecu = new Ecureuil(0);
		int pv = ecu.getPv();
		int nou = ecu.getStockNourriture();
		// Test du nombre de pv
		assertEquals("Les pv de l'ecureuil devraient être a 0", 0, pv);
		//Test de la nourriture
		assertEquals("La nourriture devrait être à 0", 0, nou);
		//Test de êtreMort
		assertEquals("L'ecureuil devrait être mort", true, ecu.etreMort());
	}
	// Test constructeur parametre PDV < 0
	@Test
	public void testConstructeurParametre3(){
		//Initialisation
		Ecureuil ecu = new Ecureuil(0);
		int pv = ecu.getPv();
		int nou = ecu.getStockNourriture();
		// Test du nombre de pv
		assertEquals("Les pv de l'ecureuil devraient être a 0", 0, pv);
		//Test de la nourriture
		assertEquals("La nourriture devrait être à 0", 0, nou);
		//Test de êtreMort
		assertEquals("L'ecureuil devrait être mort", true, ecu.etreMort());
	}
	
	// Test stockerNourriture normal
	@Test
	public void testStockerNourriture(){
		//Initialisation
		Ecureuil ecu = new Ecureuil();
		ecu.stockerNourriture(5);
		int nou = ecu.getStockNourriture();
		//Test de la nourriture
		assertEquals("La nourriture devrait être à 5", 5, nou);
	}
	
	// Test stockerNourriture nourriture<0
			@Test
			public void testStockerNourriture2(){
				//Initialisation
				Ecureuil ecu = new Ecureuil();
				ecu.stockerNourriture(7);
				ecu.stockerNourriture(-5);
				int nou = ecu.getStockNourriture();
				//Test de la nourriture
				assertEquals("La nourriture devrait être à 2", 2, nou);
			}
			
	// Test stockerNourriture noisette<0
		@Test
		public void testStockerNourriture3(){
			//Initialisation
			Ecureuil ecu = new Ecureuil();
			ecu.stockerNourriture(2);
			ecu.stockerNourriture(-5);
			int nou = ecu.getStockNourriture();
			//Test de la nourriture
			assertEquals("La nourriture devrait être à 0", 0, nou);
		}
		
	//Test passerUnJour normale
		@Test
		public void testPasserUnJour1() throws MortDeFaimException{
			Ecureuil ecu = new Ecureuil();
			ecu.stockerNourriture(5);
			ecu.passerUnjour();
			// test des parametres
			assertEquals("L'ecureuil devrait avoir 3 noisettes",3,ecu.getStockNourriture());
			assertEquals("L'ecureuil ne devrait pas avoir perdu de PDV", 5,ecu.getPv());
		}


	//Test passerUnJour manque de noisettes
		@Test
		public void testPasserUnJour2() throws MortDeFaimException{
			Ecureuil ecu = new Ecureuil();
			ecu.passerUnjour();
			// test des parametres
			assertEquals("L'ecureuil devrait avoir 0 noisette",0,ecu.getStockNourriture());
			assertEquals("L'ecureuil devrait avoir perdu des PDV", 3,ecu.getPv());
		}	
		
	//Test passerUnJour ecureuil ne passant pas la journée
		@Test(expected = animal.MortDeFaimException.class)
		public void testPasserUnJour3() throws MortDeFaimException{
			Ecureuil ecu = new Ecureuil();
			for(int i = 0; i<3;i++){
				ecu.passerUnjour();
			}
			// test des parametres
			assertEquals("L'ecureuil devrait avoir 0 noisette",0,ecu.getStockNourriture());
			assertEquals("l'ecureuil devrait etre mort", true, ecu.etreMort());
		}
		
}
