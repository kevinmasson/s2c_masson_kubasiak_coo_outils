package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import animal.*;

/**
 * Permet de tester la classe Loup du package animal
 */
public class TestLoup {

	@Test
	/**
	* Teste le constructeur par défault
	* Le loup doit avoir 30 pv et un stock de 0 par défaut
	*/
	public void testConstructeur_parDefaut ()
	{
		// preparation des donnees
		Loup l = new Loup();
		// methode testee
		int pv = l.getPv();
		int stock = l.getStockNourriture();
		boolean mort = l.etreMort();
		// validation du resultat
		assertEquals("Les pv du loup devraient être de 30",30,pv);
		assertEquals("Le stock du loup devraient être de 0",0,stock);
		assertTrue("Le loup devrait être vivant", !mort);
	}
	
	@Test
	/**
	* Teste le constructeur paramétré avec une vie nulle
	* Le loup doit avoir 0 pdv et donc être mort, son stock doit être vide aussi
	*/
	public void testConstructeur_parametre_vieNul ()
	{
		// preparation des donnees
		Loup l = new Loup(0);
		// methode testee
		int pv = l.getPv();
		int stock = l.getStockNourriture();
		boolean mort = l.etreMort();
		// validation du resultat
		assertEquals("Les pv du loup devraient être de 0",0,pv);
		assertEquals("Le stock du loup devraient être de 0",0,stock);
		assertTrue("Le loup devrait être mort", mort);
	}
	
	@Test
	/**
	* Teste le constructeur paramétré avec une vie négatif
	* Le loup doit avoir 0 pdv et donc être mort, son stock doit être vide aussi
	*/
	public void testConstructeur_parametre_vieNegatif ()
	{
		// preparation des donnees
		Loup l = new Loup(-10);
		// methode testee
		int pv = l.getPv();
		int stock = l.getStockNourriture();
		boolean mort = l.etreMort();
		// validation du resultat
		assertEquals("Les pv du loup devraient être de 0",0,pv);
		assertEquals("Le stock du loup devraient être de 0",0,stock);
		assertTrue("Le loup devrait être mort", mort);
	}
	
	@Test
	/**
	* Teste le constructeur paramétré avec une vie normal
	* Le loup doit avoir autant de pdv qu'il y a en paramètre
	* un stock nul et il doit être vivant
	*/
	public void testConstructeur_parametre_vieNormal ()
	{
		// preparation des donnees
		Loup l = new Loup(8);
		// methode testee
		int pv = l.getPv();
		int stock = l.getStockNourriture();
		boolean mort = l.etreMort();
		// validation du resultat
		assertEquals("Les pv du loup devraient être de 8",8,pv);
		assertEquals("Le stock du loup devraient être de 0",0,stock);
		assertTrue("Le loup devrait être vivant", !mort);
	}
	
	@Test
	/**
	* Teste la méthode stockerNourriture avec un paramètre nulle
	* Le stock du loup doit rester le même
	*/
	public void testStockerNourriture_valNulle()
	{
		// preparation des donnees
		Loup l = new Loup();
		// methode testee
		l.stockerNourriture(0);
		int stock = l.getStockNourriture();
		// validation du resultat
		assertEquals("Le stock du loup devraient être de 0",0,stock);
	}
	
	@Test
	/**
	* Teste la méthode stockerNourriture avec un paramètre positif
	* Le stock du loup doit augmenter de la valeur passée en paramètre
	*/
	public void testStockerNourriture_valPositif()
	{
		// preparation des donnees
		Loup l = new Loup();
		// methode testee
		l.stockerNourriture(10);
		int stock = l.getStockNourriture();
		// validation du resultat
		assertEquals("Le stock du loup devraient être de 10",10,stock);
	}
	
	@Test
	/**
	* Teste la méthode stockerNourriture avec un paramètre positif
	* Le stock du loup doit dimuner de la valeur passée en paramètre tout en restant positif
	*/
	public void testStockerNourriture_valNegatif()
	{
		// preparation des donnees
		Loup l = new Loup();
		// methode testee
		l.stockerNourriture(10);
		l.stockerNourriture(-5);
		int stock = l.getStockNourriture();
		// validation du resultat
		assertEquals("Le stock du loup devraient être de 5",5,stock);
	}
	
	@Test
	/**
	* Teste la méthode stockerNourriture avec un paramètre positif qui dépasse le seuil de 0
	* Le stock du loup doit dimuner de la valeur passée en paramètre tout en restant positif
	* son stock devra donc etre nul et non négatif
	*/
	public void testStockerNourriture_valNegatifDepasseSeuil()
	{
		// preparation des donnees
		Loup l = new Loup();
		// methode testee
		l.stockerNourriture(20);
		l.stockerNourriture(-25);
		int stock = l.getStockNourriture();
		// validation du resultat
		assertEquals("Le stock du loup devraient être de 0",0,stock);
	}
	
	@Test(expected=MortDeFaimException.class)
	/**
	* Teste la méthode passerUnJour lorsque le loup est déjà mort
	* Dans ce cas, l'exception mort de faim doit être levé et la méthode doit retourner false
	*/
	public void testPasserUnjour_dejaMort() throws MortDeFaimException
	{
		// preparation des donnees
		Loup l = new Loup(0);
		// methode testee
		boolean res = l.passerUnjour();		 
		// validation du resultat
		assertTrue("La méthode devrait passerUnJour retourner false", !res);
	}
	
	@Test
	/**
	* Teste la méthode passerUnJour lorsque le loup n'a pas assez de viande
	* Dans ce cas, le loup survie, perd 4pdv et la moitié de son stock
	*/
	public void testPasserUnjour_pasAssezViande() throws MortDeFaimException
	{
		// preparation des donnees
		Loup l = new Loup(10);
		// methode testee
		boolean res = l.passerUnjour();
		boolean mort = l.etreMort();
		int vie = l.getPv();
		// validation du resultat
		assertTrue("La méthode passerUnJour devrait retourner true", res);
		assertEquals("Les pv du loup devraient être de 6",6,vie);
		assertTrue("Le loup devrait être vivant", !mort);
	}
	
	@Test(expected=MortDeFaimException.class)
	/**
	* Teste la méthode passerUnJour lorsque le loup ne survivra pas
	* Le loup a  4 pdv et 0 viande
	* Il meurt donc
	*/
	public void testPasserUnjour_pasAssezVie() throws MortDeFaimException
	{
		// preparation des donnees
		Loup l = new Loup(2);
		// methode testee
		boolean res = l.passerUnjour();
		boolean mort = l.etreMort();
		int viande = l.getStockNourriture();
		// validation du resultat
		assertTrue("La méthode passerUnJour devrait retourner false", !res);
		assertEquals("Le stock du loup devraient être de 0",0,viande);
		assertTrue("Le loup devrait être mort", mort);
	}
	
	@Test
	/**
	* Teste la méthode passerUnJour lorsque le loup a suffisament de nourriture
	* Dans ce cas, le loup survie, et perd la moitié de son stock + 1 viande
	*/
	public void testPasserUnjour_assezNouritture() throws MortDeFaimException
	{
		// preparation des donnees
		Loup l = new Loup(10);
		// methode testee
		l.stockerNourriture(21);
		boolean res = l.passerUnjour();
		boolean mort = l.etreMort();
		int viande = l.getStockNourriture();
		int vie = l.getPv();
		// validation du resultat
		assertTrue("La méthode passerUnJour devrait retourner true", res);
		assertEquals("Les pv du loup devraient être de 10",10,vie);
		assertEquals("Le stock du loup devraient être de 10",10,viande);
		assertTrue("Le loup devrait être vivant", !mort);
	}
	
	
}
